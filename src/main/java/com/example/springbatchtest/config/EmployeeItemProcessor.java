package com.example.springbatchtest.config;

import com.example.springbatchtest.model.Employee;
import com.example.springbatchtest.model.EmployeeInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import java.util.Random;

public class EmployeeItemProcessor  implements ItemProcessor<EmployeeInput, Employee> {

    private static final Logger log = LoggerFactory.getLogger(EmployeeItemProcessor.class);

    @Override
    public Employee process(final EmployeeInput employeeInput) throws Exception {
        System.out.println(employeeInput);
        final long id = Long.parseLong(employeeInput.getId());
        final String name = employeeInput.getName();
        final String dept = employeeInput.getDept();
        final double salary = Double.parseDouble(employeeInput.getSalary());

        final Employee transformedPerson = Employee.builder().id(id).name(name).dept(dept).salary(salary).build();

        log.info("Converting (" + employeeInput + ") into (" + transformedPerson + ")");

        return transformedPerson;
    }
}

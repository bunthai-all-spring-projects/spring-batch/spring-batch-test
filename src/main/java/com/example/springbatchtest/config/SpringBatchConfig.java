package com.example.springbatchtest.config;

import com.example.springbatchtest.model.Employee;
import com.example.springbatchtest.model.EmployeeInput;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
public class SpringBatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    String[] EMPLOYEE_FIELDS = {"id", "name", "dept", "salary"};

    @Bean
    public FlatFileItemReader<EmployeeInput> reader() {
        FlatFileItemReader<EmployeeInput> e = new FlatFileItemReaderBuilder<EmployeeInput>()
            .name("personItemReader")
            .resource(new ClassPathResource("employee.csv"))
            .delimited()
            .names(EMPLOYEE_FIELDS)
            .linesToSkip(1)
            .fieldSetMapper(new BeanWrapperFieldSetMapper<EmployeeInput>() {{
                setTargetType(EmployeeInput.class);
            }})
            .build();

        return e;
    }

    @Bean
    public EmployeeItemProcessor processor() {

        return new EmployeeItemProcessor();
    }

    @Bean
    public JdbcBatchItemWriter<Employee> writer(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<Employee>()
            .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
            .sql("INSERT INTO employees (id,name,dept,salary) VALUES (:id,:name,:dept,:salary)")
            .dataSource(dataSource)
            .build();
    }


    @Bean
    public Job importUserJob(JobCompletionNotificationListener listener, Step step1) {
        return jobBuilderFactory.get("importUserJob")
            .incrementer(new RunIdIncrementer())
            .listener(listener)
            .flow(step1)
            .end()
            .build();
    }

    @Bean
    public Step step1(JdbcBatchItemWriter<Employee> writer) {
        return stepBuilderFactory.get("step1")
            .<EmployeeInput, Employee> chunk(10)
            .reader(reader())
            .processor(processor())
            .writer(writer)
            .build();
    }

}
